Simple demo of a Grails controller/view that returns JSON data from two
different sources.

Terms with why:
- The term AJAX is used hereafter to imply a just-in-time, or an on-demand User
  eXperience (UX). This is a highly desired quality in web based applications.
- AJAX is Asynchronous JavaScript and XML (or JSON, or YAML for Ruby/Rails).
- JSON is the most widely adopted and the most universal of the three formats
  because it is able to be read as a Javascript object, then manipulated as a
  Javascript variable immediately.
- controller/action may represent a URL fragment as normal Grails syntax.
  Slashes "/" between words do not always indicate a URL fragment. Only when the
  controller/action syntax is used.

The two sources:
- A controller/action is used as a closure, which acts like a function to return
  calculated JSON data. This mechanism is used by the two original examples in
  the controller. A method(), as opposed to a {closure}, is the preferred tactic
  since Groovy 1.8.x - see Grails Docs 1.1.3 Web Features, but deinerso cannot
  get the method() syntax to work with JQuery AJAX as of 2012-06-06.
  Status of backbone.js + method() tactic unknown; possible rabbit hole.
  In fact, this brings me to a major point: We will want to write services
  rather than controller/action = {closures}. Services qualify as API's, whereas
  controller/action = {closures} do not. Services in Grails are very simple,
  though understanding when to use them is sometimes not. Usually is the answer
  when the public Internet is a target platform as long as security precautions
  are taken - which have to be taken anyway. Services will be a good addition to
  this demo as a third source that includes the database.
- The built-in, default grails 1.3.7 database hsqldb without a Service.
- TODO The built-in, default grails 1.3.7 database hsqldb *with* a Service.


Four ways for a client browser or installed client application to consume the
data are also shown in these two tabs:

Demo tab (id="demo"):
- Javascript functions using jQuery AJAX to get JSON data from a
  controller/action = {closure}. Client-side HTML generation to show it.
- A Backbone.js model/collection/view to handle the same interaction.

Database Demo tab (id="db-demo"):
- Javascript functions with jQuery AJAX to select data from a domain using a
  controller/action = {closure} to get the JSON data and client-side HTML
  generation to show it.
- Using backbone.js AJAX to select data from a domain and client-side HTML
  generation to show it. The current code supporting backbone may well be
  optimized by other eyes, and by Javascript compilers, of course. Adding a
  Javascript compiler step to the build process is desirable for production
  and possibly integration tests, but not unit tests.


Important files are:
- grails-app/views/index.gsp
- grails-app/controllers/demo/DemoController.groovy
- grails-app/controllers/demo/InstitutionController.groovy
- grails-app/domains/demo/Institution.groovy
- web-app/js/demo-modified-ajax.js


Note the addition of various JavaScript and CSS resources in web-app, including:
- jQuery, http://docs.jquery.com/Downloading_jQuery
- jQuery UI, http://jqueryui.com/
- backbone.js and underscore.js at
  http://documentcloud.github.com/backbone/ and http://underscorejs.org/


Other Notes:
- grails.views.gsp.keepgenerateddir = 'target/generated' added to
  grails-app/conf/config.groovy to retain server generated code for debugging.
  I manually added the generated directory. This feature is not for production.
- Note this line in the index.gsp: <g:javascript src="demo-modified-ajax.js" />
  This is the Grails way of including Javascript files. It's shorter, too. There
  are other g:javascript attributes for other kinds of Javascript.
  Here are some handy Grails g:javascript gsp attributes:
  http://grails.org/doc/latest/ref/Tags/javascript.html
- The process I used to make the presentation on 2012-06-07 is from the
  perspective of the user and should be a formal UML Use Case Diagram, which
  we'll get to in good time. The ordering of the code is often, though not
  always, on purpose for an optimization not immediately documented or
  referenced. Regular use of JavaDoc notation in the code will reduce frequency
  of this issue. GroovyDoc notation is coming in future versions of Groovy, a
  Grails dependency, so future versions of Grails, too. Using JavaDoc has the
  benefit of automatic documentation generation right from the code and
  outputing the documentation in a variety of formats supported by JavaDoc
  (and GroovyDoc down the road, which will keep step with JavaDoc stable
  releases).

TODO:
- SOLVED 2012-06-06. How to place Javascript in an external *.js file and still
  trigger events properly? Answer: Page global variables and JQuery
  selectors that bind events must be declared in the *.gsp. All functions may be
  in an external js file even if they do more JQuery selecting. Not sure 100%
  about what kind of bind events. Is it the tab component only, or all
  components ready when the page finishes loading?
- ON HOLD 2012-06-10: Making the controller/actions methods rather than the
  default of closures breaks AJAX at present. This may be a diff between 2.0.4
  documentation and 1.3.7 docs or possibly a JQuery issue?
- DONE 2012-06-09: Make the Institution domain class more robust with supporting
  updates for the new fields.
- Using the current JQuery Tabs bind select event mechanism seems to only work
  once. The last one overrides all that went before it in the code even when a
  different tab is selected. Cannot bind select events to more than one tab using
  the current mechanism. Possible bug in JQuery or the specific mechanism syntax
  I chose/found? It's not the browser, I tested on Chrome and Firefox.
- The built-in, default grails 1.3.7 database hsqldb *with* Services.
- Unit tests
- Integration tests
- UML Use Case Diagram of the process deinerso used during a presentation
  on 2012-06-07.

Resources:
- http://en.wikipedia.org/wiki/Closure_%28computer_science%29
- https://developers.google.com/closure/

- http://en.wikipedia.org/wiki/Ajax_%28programming%29
import demo.Institution

class BootStrap {

  def init = { servletContext ->
    // Institutions
    // UNM
    def u1 =   Institution.findByName("University of New Mexico") ?:
    new Institution(
      name: "University of New Mexico",
      city: "Albuquerque",
      customerYear:"3/4/2004",
      healthSciencesInd:"Y",
      hospitalInd:"Y",
      url: "http://www.unm.edu/",
      factBookUrl: "http://www.unm.edu/" )
    .save(failOnError: true)

    // Drexel
    def u2 =   Institution.findByName("Drexel University") ?:
      new Institution(
        name: "Drexel University",
        city: "Philadelphia",
        customerYear:"3/4/2012",
        healthSciencesInd:"Y",
        hospitalInd:"Y",
        url: "http://www.drexel.edu/",
        factBookUrl: "http://www.drexel.edu/" )
      .save(failOnError: true)

    // TTU
    def u3 =  Institution.findByName("Texas Tech University") ?:
      new Institution(
        name: "Texas Tech University",
        city: "Lubbock",
        customerYear:"3/4/2012",
        healthSciencesInd:"Y",
        hospitalInd:"Y",
        url: "http://www.ttu.edu/",
        factBookUrl: "http://www.ttu.edu/" )
      .save(failOnError: true)

    assert Institution.count() == 3
  }

  def destroy = {
  }
}

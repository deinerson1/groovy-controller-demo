class UrlMappings {

  static mappings = {
    "/$controller/$action?/$id?"{
      //action = [GET:"list", POST:"save", PUT:"update", DELETE:"remove"]
      constraints {
        // apply constraints here
      }
    }

    "/"(view:"/index")
    "500"(view:'/error')
  }
}

package demo

import demo.Institution
import grails.converters.JSON

class DemoController {
  //static allowedMethods = [list:'GET']
  //def scaffold = Institution
  
  def index = {}
  
  // The original controller action.
  // Making these actions methods instead of closures doesn't seem to work.
  def multiply = {
    def factor = (params.factor && params.factor.isInteger() ) ? params.factor.toInteger() : null;

    def result = []
    if ( factor != null ) { 
      result = (0..10).collect {
        [ value:it, product:it * factor ]
      }
    }
    render result as JSON
  }
  
}

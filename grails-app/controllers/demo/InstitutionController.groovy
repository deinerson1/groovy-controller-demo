package demo
/**
 * Wrangle the view and the model via a groovy service that is Dependency
 * Injection (DI).
 */
class InstitutionController {

  /**
   * This and "def institutionService" are the only two ways to take advantage
   * of Dependency Injection (DI), in groovy.
   */
  InstitutionService institutionService

  /**
   * Redirect the default action to the list action.
   */
  def index = {
    redirect action:"list", params:params
  }

  /**
   * List action renders the InstitutionService list.
   * @params  url parameter groovy object
   * @return  false if a runtime exception occurs during the render attempt.
   * TODO Action name may interfere with scaffolding and need to be overridden(?)
   */
  def list = {
    // Use the converter to render a response and exit this method:
    def institutions = institutionService.list(params)
    //institutionService.list(params)
    institutions.render(response)

    return false

  }
}

package demo

class Institution {
  // Dependency Injection the Groovy way:
  def InstitutionService

  String name
  String city
  Date customerYear
  String healthSciencesInd
  String hospitalInd
  String url
  String factBookUrl

  String toString(){
    return "${name}"
  }

  static mapping = {
    // Default Sort is ascending:
    sort "name"
  }
}

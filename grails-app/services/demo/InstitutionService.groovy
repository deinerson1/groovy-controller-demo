package demo

import demo.Institution
import grails.converters.JSON

class InstitutionService {

  static transactional = true

  /**
   * Returns a institution JSON converter of all institutions.
   * <p>
   * @params  url parameter groovy object
   * @return  JSON Converter containing a list of all institutions
   */
  def list(params) {
    // Define a JSON converter:
    def result = Institution.list(params) as JSON

    log.debug result.toString()

    return result
  }
}
